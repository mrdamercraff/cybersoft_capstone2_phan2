function kiemTraRong(userInput, idErr) {
  if (userInput.length == 0) {
    document.getElementById(idErr).innerHTML = "Cột này không được để trống";
    return false;
  } else {
    document.getElementById(idErr).innerHTML = "";
    return true;
  }
}
function kiemTraSo(value, idErr) {
  var reg = /^\d+$/;
  let isNumber = reg.test(value);
  if (isNumber) {
    document.getElementById(idErr).innerHTML = "";
    return true;
  } else {
    document.getElementById(idErr).innerHTML = "Chỉ được nhập số";
    return false;
  }
}
function kiemTraLink(value, idErr) {
  let reg =
    /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/;
  let isLink = reg.test(value);
  if (isLink) {
    document.getElementById(idErr).innerHTML = "";
    return true;
  } else {
    document.getElementById(idErr).innerHTML = "Chỉ được nhập link";
    return false;
  }
}
