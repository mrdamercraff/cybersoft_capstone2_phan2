const BASE_URL = "https://636a13c4b10125b78fcfe079.mockapi.io";
let layDSSP = () => {
  return axios({
    method: "GET",
    url: `${BASE_URL}/quanli`,
  });
};

let themSP = (sp) => {
  return axios({
    method: "POST",
    url: `${BASE_URL}/quanli`,
    data: sp,
  });
};

let xoaSP = (id) => {
  return axios({
    method: "DELETE",
    url: `${BASE_URL}/quanli/${id}`,
  });
};

let layThongTinSP = (id) => {
  return axios({
    method: "GET",
    url: `${BASE_URL}/quanli/${id}`,
  });
};

let capNhatSP = (id, sp) => {
  return axios({
    method: "PUT",
    url: `${BASE_URL}/quanli/${id}`,
    data: sp,
  });
};
let timKiemSP = function (DSSP, TK) {
  let mangTK = [];
  mangTK = DSSP.filter(function (item) {
    return item.name.toLowerCase().indexOf(TK.toLowerCase()) >= 0;
  });
  return mangTK;
};
function turnOnLoading() {
  document.getElementById("loading").style.display = "flex";
}
function turnOffLoading() {
  document.getElementById("loading").style.display = "none";
}
