function layDSSPArr() {
  turnOnLoading();
  layDSSP().then(function (res) {
    console.log(res);
    renderDSSP(res.data);
    setLocalStorage(res.data);
    turnOffLoading();
  });
}
layDSSPArr();
let updatePara = null;
function renderDSSP(list) {
  let contentHTML = "";
  let count = 1;
  list.map(function (sp) {
    contentHTML += `
    <tr>
    <td>${count}</td>
    <td>${sp.name}</td>
    <td>${sp.price}</td>
    <td class="w-25"><img src="${sp.pic}" class="w-100" alt="" /></td>
    <td>${sp.desc}</td>
    <td>
    <button onclick="deleteSP(${sp.id})" class='btn btn-danger'>Delete</button>
    <button  onclick="getInfoSP(${sp.id})" data-toggle="modal" data-target="#myModal" class='btn btn-warning'>Edit</button>
    </td>
    </tr>
    `;
    count++;
  });
  document.getElementById("tblDanhSachSP").innerHTML = contentHTML;
}
function addSP() {
  turnOnLoading();
  let tenSP = document.getElementById("TenSP").value;
  let gia = document.getElementById("GiaSP").value;
  let hinhAnh = document.getElementById("HinhSP").value;
  let moTa = document.getElementById("MoTa").value;
  let sp = new SanPham(tenSP, gia, hinhAnh, moTa);
  let isValid = true;
  isValid = kiemTraRong(tenSP, "spanTenSP");
  isValid =
    isValid & (kiemTraRong(gia, "spanGiaSP") && kiemTraSo(gia, "spanGiaSP"));
  isValid =
    isValid &
    (kiemTraRong(hinhAnh, "spanHinhSP") && kiemTraLink(hinhAnh, "spanHinhSP"));
  isValid = isValid & kiemTraRong(moTa, "spanMoTa");

  if (isValid) {
    themSP(sp)
      .then(function (res) {
        layDSSPArr();
        document.querySelector("#myModal .close").click();
        turnOffLoading();
      })
      .catch(function (err) {
        console.log(err);
      });
  } else {
    {
      turnOffLoading();
    }
  }
}

function deleteSP(id) {
  xoaSP(id)
    .then(function (res) {
      layDSSPArr();
    })
    .catch(function (err) {
      console.log(err);
    });
}

function btnThemSP() {
  document.getElementById("updateSP").classList.remove("d-block");
  document.getElementById("updateSP").classList.add("d-none");
  document.getElementById("addSP").classList.remove("d-none");
  document.getElementById("addSP").classList.add("d-block");
}
function getInfoSP(id) {
  layThongTinSP(id)
    .then(function (res) {
      document.getElementById("TenSP").value = res.data.name;
      document.getElementById("GiaSP").value = res.data.price;
      document.getElementById("HinhSP").value = res.data.pic;
      document.getElementById("MoTa").value = res.data.desc;

      document.getElementById("addSP").classList.remove("d-block");
      document.getElementById("addSP").classList.add("d-none");
      document.getElementById("updateSP").classList.remove("d-none");
      document.getElementById("updateSP").classList.add("d-block");
      updatePara = res.data.id;
    })
    .catch(function (err) {
      console.log(err);
    });
}

function updateSP(id) {
  id = updatePara;
  let tenSP = document.getElementById("TenSP").value;
  let gia = document.getElementById("GiaSP").value;
  let hinhAnh = document.getElementById("HinhSP").value;
  let moTa = document.getElementById("MoTa").value;
  let sp = new SanPham(tenSP, gia, hinhAnh, moTa);
  isValid = kiemTraRong(tenSP, "spanTenSP");
  isValid =
    isValid & (kiemTraRong(gia, "spanGiaSP") && kiemTraSo(gia, "spanGiaSP"));
  isValid =
    isValid &
    (kiemTraRong(hinhAnh, "spanHinhSP") && kiemTraLink(hinhAnh, "spanHinhSP"));
  isValid = isValid & kiemTraRong(moTa, "spanMoTa");
  if (isValid) {
  }
  capNhatSP(id, sp)
    .then(function (res) {
      layDSSPArr();
      document.querySelector("#myModal .close").click();
    })
    .catch(function (err) {
      console.log(err);
    });
}
function setLocalStorage(SParr) {
  localStorage.setItem("DSSP", JSON.stringify(SParr));
}
function getLocalStorage() {
  let mangKQ = JSON.parse(localStorage.getItem("DSSP"));
  return mangKQ;
}
document.getElementById("basic-addon2").addEventListener("click", function () {
  let mangKQ = getLocalStorage();
  let timSpArr = [];
  let chuoiTK = document.getElementById("inputTK").value;
  timSpArr = timKiemSP(mangKQ, chuoiTK);
  renderDSSP(timSpArr);
});
